# Copyright 2009 Hong Hao <oahong@gmail.com> ::exherbo-cn
# Copyright 2012, 2013, 2014 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require xfce [ module=apps autotools="true" intltoolize="true" require_dev_tools="true" ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Xfburn CD/DVD/BluRay burning tool from XFCE"
DESCRIPTION="
Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can blank CD/DVD(-RW)s,
burn and create iso images, audio CDs, as well as burn personal compositions of data to either CD
or DVD.
"
HOMEPAGE="https://goodies.xfce.org/projects/applications/${PN}"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    ( linguas: ar ast bg ca cs da de el en en_AU en_GB es et eu fi fr gl he hr hu id is it ja kk ko
               lt lv ms nb nl nn oc pa pl pt pt_BR ru si sk sq sr sv te th tr ug uk ur ur_PK vi
               zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
    build+run:
        app-cdr/libburn[>=0.4.2]
        app-cdr/libisofs[>=0.6.2]
        dev-libs/dbus-glib:1[>=0.34]
        dev-libs/glib:2[>=2.6.0]
        gnome-desktop/libgudev[>=145]
        media-plugins/gst-plugins-base:1.0[>=1.0.0]
        x11-libs/gdk-pixbuf:2.0[>=2.22.0]
        x11-libs/gtk+:2[>=2.10.0]
        xfce-base/exo[>=0.3.4]
        xfce-base/libxfce4ui[>=4.8.0]
    suggestion:
        (
            media-plugins/gst-libav
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:ogg][gstreamer_plugins:vorbis]
            media-plugins/gst-plugins-good:1.0[gstreamer_plugins:flac]
            media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:mpeg2]
        ) [[
            *description = [ Dependencies for burning compressed music files as an audio cd ]
            *group-name  = [ audio-cd ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gstreamer
    --enable-gudev
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    debug
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

