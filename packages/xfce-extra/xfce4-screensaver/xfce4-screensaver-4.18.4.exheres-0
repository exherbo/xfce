# Copyright 2024 Tristan Charbonneau <tristan.charbonneau@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require xfce [ module=apps autotools=true require_dev_tools=true ]

SUMMARY="Screen saver and locker that aims to have simple, sane, secure defaults and be well integrated with the desktop"
HOMEPAGE="http://goodies.xfce.org/projects/apps/${PN}"
MYOPTIONS="
    consolekit [[
        description = [ Integration with ConsoleKit ]
    ]]
    pam
    ( providers: elogind systemd ) [[
        description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

SLOT="0"
PLATFORMS="~amd64"
DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.50.0]
        dev-libs/dbus-glib:1
        dev-libs/libglvnd
        gnome-desktop/libwnck:3.0[>=3.20]
        sys-apps/dbus[>=0.30]
        x11-apps/xrandr[>=1.3]
        x11-apps/xscreensaver
        x11-libs/gtk+:3[>=3.24.0]
        x11-libs/libX11[>=1.0]
        x11-libs/libXext
        x11-libs/libxklavier[>=5.2]
        x11-libs/libXScrnSaver
        xfce-base/garcon[>=0.5.0]
        xfce-base/libxfce4ui[>=4.18.4.0]
        xfce-base/libxfce4util[>=4.12.1]
        xfce-base/xfce4-panel[>=4.12.0]
        xfce-base/xfconf[>=4.12.1]
        consolekit? (
            sys-apps/dbus
            sys-auth/ConsoleKit2
        )
        pam? (
            sys-libs/pam
        )
    run:
        providers:elogind? ( sys-auth/elogind[pam] )
        providers:systemd? ( sys-apps/systemd )
    suggestion:
        providers:elogind? ( sys-auth/pam_rundir ) [[
            description = [ Set XDG_RUNTIME_DIR automatically on login without systemd ]
        ]]
"

## calls to pkg-config must always be prefixed
src_prepare() {
    default
    edo sed -e "s/pkg-config/$(exhost --tool-prefix)&/" \
            -i configure.ac
    autotools_src_prepare
}

src_configure()
{
    myconf=(
        --enable-locking
        --enable-pam
        --with-libgl
        --with-shadow
        $(option_with consolekit console-kit)
        $(option_with providers:systemd systemd)
        $(option_with providers:elogind elogind)
        $(option_enable pam)
    )

    econf "${myconf[@]}"
}
